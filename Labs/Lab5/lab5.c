#include <stdio.h>
#define MAXLINE 1000 /* maximum length of string */

/* function prototype */
void reverse(char source[], char target[]);
long string_length(char s[]);

int main(void) {
  char original[] = "This is a test: can you print me in reverse character order?";
  char reversed[MAXLINE];

  printf("%s\n", original);
  reverse(original, reversed);
  printf("%s\n", reversed);
  return 0;

}

/* reverse the order of characters in 'source', write to 'target'.
   Assume 'target' is big enough. */
void reverse(char source[], char target[]) {

	int length;
	int j;
	int c = 0;
	char temp;

	length = string_length(source);
	j = length - 1;

	while (0 < j){
		temp = source[c];
		target[c] = source[j];
		target[j] = temp;
		c++;
		j--;

	}

	target[c+1] = '\0';
	if (length ==0){
		target[0]='\0';
	}

}

long string_length(char s[]){

	long nc=0;

	while(s[nc] != '\0'){
		nc++;
	}

	return nc;

}
