#include <stdio.h>

int    Celsius;
double Farenheit;

int main(void){

	for(Celsius = -30; Celsius < 31; Celsius+=2){

		Farenheit = Celsius * 9 / 5. + 32;
		printf("%3d = %5.1f\n", Celsius, Farenheit);
	}

	return 0;
}
