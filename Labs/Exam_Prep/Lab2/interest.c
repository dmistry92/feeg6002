#include <stdio.h>

int main(void){

	int s       = 1000; /* Sum borrowed in pounds */
	double debt    = s;    /* Actual debt in pounds */
	double rate = 0.03; /* Interest rate of 3% */
	double interest; 
	double total_interest = 0; 
	double frac; 
	int month   = 0;    /* Month */ 

	for(month = 1; month <=24; month++){

		interest = debt * rate; /* Interest this month */
		debt = debt + interest;
		total_interest += interest;
		frac = (total_interest/s)*100;

		printf("month %2d: debt=%7.2f, interest=%5.2f, total_interest=%7.2f, frac=%6.2f%%\n", month, debt, interest, total_interest, frac);

	}

	return 0;
}
