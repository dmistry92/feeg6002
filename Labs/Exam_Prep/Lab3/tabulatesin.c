#include <stdio.h>
#include <math.h>

#define XMAX 10
#define XMIN 1
#define N 10

int main(void){
	
	double x;
	double y;
	int    i;

	for(i=0; i<N; i++){

		x = XMIN + (XMAX - XMIN) / (N - 1.) * i;
		y = sin(x);

		printf("%9.6f %9.6f\n", x, y);
	}


	return 0;
}
