# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 00:15:13 2015

@author: dipenmistry
"""

import sympy as sp
import numpy as np 


def central_differencing():
    """ Takes as an input a positive, even integer order. The function derives from the Taylor series expansion,
    the coefficients cn, and from those coefficients generate a function df. Only the function df should be returned.
    df should take as an input an arbitrary function of one variable f, the point x at which the derivative is to be 
    evaluated, and the step length h.
    
    Dipen Mistry, dm8g12@soton.ac.uk, 4/12/2015 """
    
    
    return None
    
    