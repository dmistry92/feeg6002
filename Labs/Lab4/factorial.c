#include <limits.h> /* Limits for integers */
#include <float.h>  /* limits for floats */
#include <math.h>   /*     */ 
#include <stdio.h>


long maxlong(void){
    return LONG_MAX;
}

double upper_bound(long n){
    if (n<6){
        return 719;
    }
        
    else {
     return pow((n/2.), n);   
    }      
}

long factorial(long p){
    if (p<0){
        return -2;
    }
    else { 
    
    return pow((p/2.), p);
    }
}
int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
    
    return 0;
}
