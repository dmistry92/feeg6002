/* FEEG6002 Lab 6. Stripping off spaces from the front of a string */
#include <stdio.h>

void lstrip(char s[]){

	int len  = 0;
	int temp = 0;
	int x;

	

	while(*(s+temp) == '\n' || *(s+temp) == '\t' || *(s+temp) == ' '){
		temp++;
	}

	printf("Test 1: s = '%s'\n", s);
	printf("Test 1: temp = '%d'\n", temp);

	while(s[len] != '\0'){
		len++;
	}

	printf("String: '%d'\n",len);

	printf("Test 2: s = '%s'\n", s);

	for(x=0; x<len-temp; x++){
		*(s+x) = *(s+x+temp);
	}

	*(s+x) = '\0'; 

}

int main(void){
	char test1[] = "   Hello World";

  	printf("Original string reads  : |%s|\n", test1);
  	lstrip(test1);
  	printf("l-stripped string reads: |%s|\n", test1);

	return 0;

}
