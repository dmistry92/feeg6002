/* Laboratory 6, SESG6025, 2013/2014, Template */

#include <stdio.h>

/* Function void rstrip(char s[])
modifies the string s: if at the end of the string s there are one or more spaces,
then remove these from the string.

The name rstrip stands for Right STRIP, trying to indicate that spaces at the 'right'
end of the string should be removed.
*/

int len;

void rstrip(char s[]) {
    /* to be implemented */

	int len = 0;
    while(s[len] != '\0'){
    	len++;
    }

    printf("Test1: %d\n", len);

    len--;

    printf("Test2: %d\n", len);
    printf("Test2: '%s'\n", s);

    while(*(s+len) == '\n'||*(s+len) == '\t'||*(s+len) == ' '){
		len--;
		printf("Test4: %d\n", len);
		
	}

	printf("Test5: '%s'\n", s);

	*(s+len+1) = '\0'; 

	printf("Test6: '%s'\n", s);
}


int main(void) {
  char test1[] = "Hello World   ";

  printf("Original string reads  : |%s|\n", test1);
  rstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);

  return 0;
}
