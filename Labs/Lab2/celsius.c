#include <stdio.h>

int main (void){
	
	int 	T_Celsius;
	double 	T_Fahrenheit;
	int 	N = 30;

	for (T_Celsius = -30; T_Celsius<=N; T_Celsius+=2){

		T_Fahrenheit = T_Celsius*9/5.+32;

		printf("%3d = %5.1f\n", T_Celsius, T_Fahrenheit);
	}
	return 0;
}
