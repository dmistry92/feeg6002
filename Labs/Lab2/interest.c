/* interest.c -  a program to show how debt grows from initially 1000GBP over 24 months
   Dipen Mistry 9th October 2015 
*/

#include <stdio.h>

int main (void){
	
	int   s    = 1000;   /* sum borrowed in GBP */
	float debt = s;	     /* actual debt */
	float rate = 0.03;   /* interest rate, i.e. 3% */
	float interest;		 /* declare interest */
	int   month;		 /* declare month */
	float total_interest = 0;
	float frac;

	for (month = 1; month <= 24; month++) {
		interest       = debt * rate;
		debt           = debt + interest;
		total_interest += interest;
		frac           = (total_interest/s)*100;

		printf("month %2d: debt=%7.2f, interest=%5.2f, total_interest=%7.2f, frac=%6.2f%%\n", month, debt, interest, total_interest, frac);
	}

	return 0;
}
