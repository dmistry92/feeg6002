#include <stdio.h>
#include <math.h>

#define N 	  10  /* Use N points */
#define XMIN  1.0 /* x = xmin */
#define XMAX 10.0 /* x = xmax */

int main (void){

	double x;
	double y;
	int	   i; 

	for (i=0; i<N; i++){

		x = XMIN + (XMAX - XMIN) / (N - 1.) * i;
		y = sin(x);

		printf("%9.6f %9.6f\n", x, y); 
	} 



}
